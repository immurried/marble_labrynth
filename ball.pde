class Ball {
  PVector pos;
  float rad = 3;
  PVector col;
  
  PVector vel;
  PVector acc;
  
  float accelSpeed = 15;
  
  Ball(PVector spawn)
  {
     col = new PVector(0, 200, 100);
     pos = new PVector(spawn.x, rad, spawn.z);
     vel = new PVector(0,0,0);
     acc = new PVector(0,0,0);
  }
 
  
  void Draw()
  {
    pushMatrix();
    fill(col.x, col.y, col.z);
    translate(pos.x, pos.y, pos.z);
    sphere(rad);
    popMatrix();
  }
  
  void Reset(PVector spawn)
  {
     pos = new PVector(spawn.x, rad, spawn.z);
     vel = new PVector(0,0,0);
     acc = new PVector(0,0,0);
     bWinScreen = false;
     totalSec = 0;
  }
  
  void Update()
  {    
    vel.x += acc.x * deltaTime * accelSpeed * abs(cam.pos.x / cam.maxX);
    vel.y += acc.y * deltaTime * accelSpeed;
    vel.z += acc.z * deltaTime * accelSpeed * abs(cam.pos.z / cam.maxZ);
    
    pos.x += vel.x * deltaTime;
    pos.y += vel.y * deltaTime;
    pos.z += vel.z * deltaTime;
  }
}
