class Board {
  
  // walls are based on a grid
  // will start out on a 13x13 board
  // each cell will be 8 with 2 for a buffer
  
  // because we want to keep the game using primitives we wont actually use real holes
  // rather we will paint on circles to define an area that the player will lose if the center of the ball enters this area
  
  PVector size;
  PVector pos;
  PVector col;
  
  float cellSize = 10;
  float cellBuffer = 2;
  
  ArrayList<Wall> walls;
  ArrayList<Hole> holes;
  Hole goal;
  
  Board()
  {
    size = new PVector(130, 30, 130);
    pos = new PVector(0, -size.y / 2.0, 0);
    col = new PVector(180, 0, 180);
  }
  
  void Update()
  {
  }
  
  void Draw()
  {
     pushMatrix();
     translate(pos.x, pos.y, pos.z);
     fill(col.x, col.y, col.z);
     box(size.x, size.y, size.z);
     popMatrix();
     
     for (int i = 0; i < walls.size(); i++)
     {
        walls.get(i).Draw();
     }
     
     for (int i = 0; i < holes.size(); i++)
     {
        holes.get(i).Draw();
     }
     
     goal.Draw();
  }
  
  private void Populate()
  {
    walls = levelOne.walls;
    holes = levelOne.holes;
    goal = levelOne.goal;
  }
  
}
