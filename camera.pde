class Camera {

  PVector pos = new PVector(0, 200, 0);
  private float maxX = 100;
  private float maxZ = 100;
  PVector input = new PVector(0, 0, 0);
  private float speed = 500;

  Camera()
  {
    camera(pos.x, pos.y, pos.z, 0, 0, 0, 0, 0, -1);
  }

  void Update()
  {
    if (input.x == 0)
      pos.x = pos.x/4 * 3;
    if (input.y == 0)
      pos.z = pos.z/4 * 3;

    pos.x += input.x * speed * deltaTime;
    pos.z += input.y * speed * deltaTime;

    if (pos.x < -maxX) pos.x = -maxX;
    else if (pos.x > maxX) pos.x = maxX;
    if (pos.z < -maxZ) pos.z = -maxZ;
    else if (pos.z > maxZ) pos.z = maxZ;

    camera(pos.x, pos.y, pos.z, 0, 0, 0, 0, 0, -1);
  }
}
