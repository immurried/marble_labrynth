class Hole {
  
  PVector pos;
  PVector col;
  float rad = 4;
  float a = 0;
  
  Hole(int gridX, int gridY)
  {   
     pos = new PVector(gridX * board.cellSize - (board.size.x / 2.0) + rad + (board.cellBuffer / 2.0),
     1, 
     gridY * board.cellSize - (board.size.z / 2.0) + rad + (board.cellBuffer / 2.0));
     col = new PVector(0, 0, 0); 
  }
  
  void Draw()
  {
      pushMatrix();
      translate(pos.x, pos.y, pos.z);
      rotateX(PI / 2.0);
      fill(col.x, col.y, col.z);
      circle(0, 0, rad*2);
      popMatrix();
  }
  
}
