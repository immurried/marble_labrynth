void keyPressed()
{
  if (key == 'r' || key == 'R')
  {
    bWinScreen = false;
    ball.Reset(levelOne.spawnPoint);
  }
  
  if (key == 'a' || key == 'A' || keyCode == LEFT)
  {
    cam.input.x += 1;
    ball.acc.x += -1;
  }

  if (key == 'd' || key == 'D' || keyCode == RIGHT)
  {
    cam.input.x += -1;
    ball.acc.x += 1;
  }

  if (key == 'w' || key == 'W' || keyCode == UP)
  {
    cam.input.y += -1;
    ball.acc.z += 1;
  }

  if (key == 's' || key == 'S' || keyCode == DOWN)
  {
    cam.input.y += 1;
    ball.acc.z += -1;
  }
}

void keyReleased()
{
  if (key == 'a' || key == 'A' || keyCode == LEFT)
  {
    cam.input.x += -1;
    ball.acc.x += 1;
  }

  if (key == 'd' || key == 'D' || keyCode == RIGHT)
  {
    cam.input.x += 1;
    ball.acc.x += -1;
  }

  if (key == 'w' || key == 'W' || keyCode == UP)
  {
    cam.input.y += 1;
    ball.acc.z += -1;
  }

  if (key == 's' || key == 'S' || keyCode == DOWN)
  {
    cam.input.y += -1;
    ball.acc.z += 1;
  }
}
