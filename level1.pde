class LevelOne{
 
  ArrayList<Wall> walls;
  ArrayList<Hole> holes;
  
  Hole goal;
  
  PVector spawnPoint = new PVector(10, 0, 60);
  
  LevelOne()
  {
    AddWalls();
    AddHoles();
    goal = new Hole(12, 6);
    goal.col = new PVector(0, 255, 0);
  }
  
  void AddWalls()
  {
    walls = new ArrayList<Wall>();
    
    // out walls
    walls.add( new Wall(false, 13, 0, 0));
    walls.add( new Wall(false, 13, 0, 13));
    walls.add( new Wall(true, 13, 0, 0));
    walls.add( new Wall(true, 13, 13, 0));
    
    walls.add( new Wall(false, 1, 8, 1));
    
    walls.add( new Wall(false, 1, 5, 3));
    
    walls.add( new Wall(false, 2, 1, 4));
    walls.add( new Wall(false, 1, 9, 4));
    walls.add( new Wall(false, 1, 6, 4));
    
    walls.add( new Wall(false, 1, 12, 6));
    
    walls.add( new Wall(false, 1, 9, 7));
    
    walls.add( new Wall(false, 2, 1, 8));
    
    walls.add( new Wall(false, 1, 5, 9));
    walls.add( new Wall(false, 2, 8, 9));
    walls.add( new Wall(false, 2, 0, 9));
    
    walls.add( new Wall(false, 1, 1, 11));
    walls.add( new Wall(false, 1, 8, 11));
    walls.add( new Wall(false, 1, 10, 11));
    
    walls.add( new Wall(false, 2, 0, 12));
    walls.add( new Wall(false, 3, 5, 12));
    
    
    walls.add( new Wall(true, 1, 4, 0));
    
    walls.add( new Wall(true, 3, 3, 1));
    walls.add( new Wall(true, 2, 5, 1));
    walls.add( new Wall(true, 1, 6, 1));
    walls.add( new Wall(true, 2, 7, 1));
    walls.add( new Wall(true, 3, 10, 1));
    walls.add( new Wall(true, 1, 11, 1));
    
    walls.add( new Wall(true, 1, 4, 2));
    
    walls.add( new Wall(true, 3, 11, 3));
    walls.add( new Wall(true, 3, 8, 3));
    
    walls.add( new Wall(true, 4, 6, 4));
    walls.add( new Wall(true, 2, 9, 4));
    
    walls.add( new Wall(true, 2, 1, 5));
    walls.add( new Wall(true, 2, 3, 5));
    walls.add( new Wall(true, 2, 4, 5));
    walls.add( new Wall(true, 2, 5, 5));
    walls.add( new Wall(true, 1, 8, 5));
    walls.add( new Wall(true, 3, 10, 5));
    
    
    walls.add( new Wall(true, 2, 12, 6)); 
    walls.add( new Wall(true, 1, 7, 6));
    
    walls.add( new Wall(true, 6, 8, 7));
    walls.add( new Wall(true, 4, 11, 7));
    
    walls.add( new Wall(true, 2, 6, 9));
    walls.add( new Wall(true, 2, 3, 8));
    walls.add( new Wall(true, 1, 4, 8));
    walls.add( new Wall(true, 1, 7, 8));
    
   
    walls.add( new Wall(true, 1, 12, 10));
    
    walls.add( new Wall(true, 1, 10, 10));
    
    walls.add( new Wall(true, 1, 4, 10));
    walls.add( new Wall(true, 2, 5, 10));
    walls.add( new Wall(true, 1, 7, 10));
    
    walls.add( new Wall(true, 1, 3, 11));
    
    walls.add( new Wall(true, 1, 4, 12));
    walls.add( new Wall(true, 1, 11, 12));
    walls.add( new Wall(true, 1, 12, 12));
  }
  
  void AddHoles()
  {
     holes = new ArrayList<Hole>();
     
     holes.add( new Hole(4,0));
     holes.add( new Hole(8,0));
     holes.add( new Hole(11,0));
     
     holes.add( new Hole(1,1));
     holes.add( new Hole(5,1));
     holes.add( new Hole(8,1));
     
     holes.add( new Hole(0,2));
     holes.add( new Hole(3,2));
     holes.add( new Hole(7,2));
     holes.add( new Hole(12,2));
     
     holes.add( new Hole(2,3));
     holes.add( new Hole(5,3));
     holes.add( new Hole(10,3));
     holes.add( new Hole(11,3));
     
     holes.add( new Hole(3,4));
     holes.add( new Hole(8,4));
     
     holes.add( new Hole(0,5));
     holes.add( new Hole(12,5));
     
     holes.add( new Hole(4,6));
     holes.add( new Hole(7,6));
     
     holes.add( new Hole(2,7));
     holes.add( new Hole(9,7));
     holes.add( new Hole(10,7));
     holes.add( new Hole(11,7));
     
     holes.add( new Hole(4,8));
     holes.add( new Hole(5,8));
     holes.add( new Hole(6,8));
     holes.add( new Hole(8,8));
     
     holes.add( new Hole(8,9));
     
     holes.add( new Hole(2,10));
     holes.add( new Hole(3,10));
     holes.add( new Hole(4,10));
     holes.add( new Hole(10,10));
     
     holes.add( new Hole(7,11));
     holes.add( new Hole(12, 11));
     
     holes.add( new Hole(0,12));
     holes.add( new Hole(8,12));
     holes.add( new Hole(10,12));
     holes.add( new Hole(11,12));
  }

}
