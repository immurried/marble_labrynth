/*
    Created by Zachary Dingwall for the use of the Macquarie University Mini Console
*/

Ball ball;
Board board;
LevelOne levelOne;
Camera cam;
Physics physics;

float deltaTime;
int prevMillis;

boolean bWinScreen = false;
float totalSec;

void setup() {
  size(600, 480, P3D);
  //noSmooth();
  smooth(16);
  perspective();

  physics = new Physics();

  cam = new Camera();

  board = new Board();
  levelOne = new LevelOne();
  ball = new Ball(levelOne.spawnPoint);
  board.Populate();
}

void draw() {
  background(0);
  noStroke();

  ambientLight(100, 100, 100, cam.pos.x, cam.pos.y, cam.pos.z);
  directionalLight(100, 100, 100, -1, -1, -1);
  if (!bWinScreen)
  {
    spotLight(255, 255, 255, 
    cam.pos.x, cam.pos.y, cam.pos.z, 
    (ball.pos.x - cam.pos.x) + 5, ball.pos.y - cam.pos.y, (ball.pos.z - cam.pos.z) + 5, 
    PI/20, 500);
    
    totalSec += deltaTime;
  }
   else {
      directionalLight(100, 100, 100, 0, -1, 0);
   }

  cam.Update();
  ball.Update();
  board.Update();

  ball.Draw();
  board.Draw();
  if (bWinScreen) DrawWinScreen();

  WallCollisionResolution();
  HoleCollisionResolution();
  GoalCollisionResolution();

  //DrawAxis();
  
  deltaTime = (millis() - prevMillis) / 1000.0;
  prevMillis = millis();
}

void WallCollisionResolution()
{
  for (int i = 0; i < board.walls.size(); i++)
  {
    PVector p = physics.sphereBoxCollision(ball, board.walls.get(i));
    ball.pos.x += p.x;
    ball.pos.z += p.z;
  }
}

void HoleCollisionResolution()
{
  for (int i = 0; i < board.holes.size(); i++)
  {
    if (physics.sphereCircleCollision(ball, board.holes.get(i)))
    {
      ball.Reset(levelOne.spawnPoint);
    }
  }
}

void GoalCollisionResolution()
{
  if (physics.sphereCircleCollision(ball, board.goal))
  {
    bWinScreen = true;
  }
}

void DrawWinScreen()
{
  pushMatrix();
  translate(0, cam.pos.y - 150, 0);
  rotateX(-PI / 2.0);
  fill(255);
  rectMode(CENTER);
  rect(0, 0, 120, 60, 5);
  
  String s = "You Win!";
  String r = "Press R to restart";
  
  int min = (int)totalSec / 60;
  int sec = (int)totalSec % 60;
  
  String t = min + ":" + sec;
  
  fill(0);
  textMode(SHAPE);
  textAlign(CENTER, CENTER);
  
  textSize(12);
  text(s, 0, -15, 1);
  
  textSize(8);
  text(t, 0, 0, 1);
  
  textSize(8);
  text(r, 0, 20, 1);
  
  popMatrix();
}

void DrawAxis()
{
  pushMatrix();
  fill(255);
  translate(0, 0, 0);
  sphere(5);
  popMatrix();

  pushMatrix();
  fill(255, 0, 0);
  translate(50, 0, 0);
  sphere(5);
  popMatrix();

  pushMatrix();
  fill(0, 0, 255);
  translate(0, 50, 0);
  sphere(5);
  popMatrix();

  pushMatrix();
  fill(0, 255, 0);
  translate(0, 0, 50);
  sphere(5);
  popMatrix();
}
