class Physics{
  
  float dampening = 0.25;
 
  Physics(){
  }
  
  PVector sphereBoxCollision(Ball ball, Wall wall)
  {
    float xDistance = ball.pos.x - (wall.pos.x + wall.size.x / 2.0) + 1;
    float zDistance = ball.pos.z - (wall.pos.z + wall.size.z / 2.0) + 1;
    
    // 1---2
    // |   |
    // 3---4
    
    PVector cornVecOne;
    PVector cornVecTwo;
    PVector cornVecThree;
    PVector cornVecFour;
    
    if (wall.vert)
    {
    
      cornVecOne = new PVector (ball.pos.x - (wall.pos.x - wall.size.x / 2.0), 0,
                                            ball.pos.z - (wall.pos.z + wall.size.z));
                               
      cornVecTwo = new PVector (ball.pos.x - (wall.pos.x + wall.size.x / 2.0), 0,
                                          ball.pos.z - (wall.pos.z + wall.size.z));
                                        
      cornVecThree = new PVector (ball.pos.x - (wall.pos.x - wall.size.x / 2.0), 0,
                                          ball.pos.z - (wall.pos.z));
                                        
      cornVecFour = new PVector (ball.pos.x - (wall.pos.x + wall.size.x / 2.0), 0,
                                          ball.pos.z - (wall.pos.z));
    }
    else
    {
      cornVecOne = new PVector (ball.pos.x - (wall.pos.x), 0,
                                            ball.pos.z - (wall.pos.z + wall.size.z / 2.0));
                               
      cornVecTwo = new PVector (ball.pos.x - (wall.pos.x + wall.size.x), 0,
                                            ball.pos.z - (wall.pos.z + wall.size.z / 2.0));
                                        
      cornVecThree = new PVector (ball.pos.x - (wall.pos.x), 0,
                                          ball.pos.z - (wall.pos.z - wall.size.z / 2.0));
                                        
      cornVecFour = new PVector (ball.pos.x - (wall.pos.x + wall.size.x), 0,
                                          ball.pos.z - (wall.pos.z - wall.size.z / 2.0));
    }
                                        
    float magnitudeOne = sqrt(sq(cornVecOne.x) + sq(cornVecOne.z));
    float magnitudeTwo = sqrt(sq(cornVecTwo.x) + sq(cornVecTwo.z));
    float magnitudeThree = sqrt(sq(cornVecThree.x) + sq(cornVecThree.z));
    float magnitudeFour = sqrt(sq(cornVecFour.x) + sq(cornVecFour.z));
    
    if (magnitudeOne < ball.rad)
    {
      ball.vel.x += -ball.vel.x * dampening;
      ball.vel.z += -ball.vel.z * dampening;
      return new PVector((cornVecOne.x / magnitudeOne) * 0.2, 0, (cornVecOne.z / magnitudeOne) * 0.2);
    }
    if (magnitudeTwo < ball.rad)
    {
      ball.vel.x += -ball.vel.x * dampening;
      ball.vel.z += -ball.vel.z * dampening;
      return new PVector(cornVecTwo.x / magnitudeTwo * 0.2, 0, cornVecTwo.z / magnitudeTwo * 0.2);
    }
    if (magnitudeThree < ball.rad)
    {
      ball.vel.x += -ball.vel.x * dampening;
      ball.vel.z += -ball.vel.z * dampening;
      return new PVector(cornVecThree.x / magnitudeThree * 0.2, 0, cornVecThree.z / magnitudeThree * 0.2);
    }
    if (magnitudeFour < ball.rad)
    {
      ball.vel.x += -ball.vel.x * dampening;
      ball.vel.z += -ball.vel.z * dampening;
      return new PVector(cornVecFour.x / magnitudeFour * 0.2, 0, cornVecFour.z / magnitudeFour * 0.2);
    }
    
    
    if (abs(xDistance) < (wall.size.x / 2.0) + ball.rad &&
    ball.pos.z >= wall.pos.z &&
    ball.pos.z <= wall.pos.z + wall.size.z)
    {
      ball.vel.x = -ball.vel.x * dampening;
      
      PVector p;
      
      if (xDistance < 0)
      {
        p = new PVector(-((wall.size.x / 2.0) + ball.rad - abs(xDistance)), 0, 0);
      }
      else p = new PVector((wall.size.x / 2.0) + ball.rad - abs(xDistance), 0, 0);
      
      return p;
    }
    else if (abs(zDistance) < (wall.size.z / 2.0) + ball.rad &&
    ball.pos.x >= wall.pos.x &&
    ball.pos.x <= wall.pos.x + wall.size.x)
    {
      ball.vel.z = -ball.vel.z * dampening;
      
      PVector p;
      
      if (zDistance < 0)
      {
        p = new PVector(0, 0, -((wall.size.z / 2.0) + ball.rad - abs(zDistance)));
      }
      else p = new PVector(0, 0, ((wall.size.z / 2.0) + ball.rad) - abs(zDistance));
      
      return p;
    }
    
    
    return new PVector(0, 0, 0);
  }
  
  boolean sphereCircleCollision(Ball ball, Hole hole)
  {
    float distance = sqrt( sq(ball.pos.x - hole.pos.x) + sq(ball.pos.z - hole.pos.z));
    
    return distance < hole.rad;
  } 
}
