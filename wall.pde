class Wall {
  
  boolean vert;
  float len;
  PVector col;
  PVector size;
  
  private PVector pos;
  
  Wall (boolean v, float l, int gridX, int gridY)
  {
      vert = v;
      len = l;
      col = new PVector(255, 255, 255);
      pos = new PVector(gridX * board.cellSize - (board.size.x / 2.0), 0, gridY * board.cellSize - (board.size.z / 2.0));
      if (vert) size = new PVector(board.cellBuffer, 20, (float)len * board.cellSize);
      else size = new PVector((float)len * board.cellSize, 20, board.cellBuffer);  
  }
  
  void Draw()
  {
    pushMatrix();
    fill(col.x, col.y, col.z);    
    if (vert){
      translate(pos.x, pos.y, pos.z + (size.z / 2.0));
      box(size.x, 20, size.z);
    }
    else{
      translate(pos.x + (size.x /2.0), pos.y, pos.z);
      box(size.x, 20, size.z);
    }
    popMatrix();
  }
}
